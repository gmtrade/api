<?php

namespace common\fixtures;

use yii\test\ActiveFixture;

abstract class BaseFixture extends ActiveFixture
{
    protected function getData()
    {
        if (!empty($this->dataFile)) {
            $class = new \ReflectionClass($this);
            $dataFile = dirname($class->getFileName()) . '/data/' . $this->dataFile;

            return is_file($dataFile) ? require $dataFile : [];
        }

        return parent::getData();
    }
}
