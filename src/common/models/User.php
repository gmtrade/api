<?php

namespace common\models;

use dektrium\user\helpers\Password;
use dektrium\user\models\User as BaseUser;
use filsh\yii2\oauth2server\models\OauthAccessTokens;
use OAuth2\Storage\UserCredentialsInterface;

class User extends BaseUser implements UserCredentialsInterface
{

    /**
     * Grant access tokens for basic user credentials.
     *
     * Check the supplied username and password for validity.
     *
     * You can also use the $client_id param to do any checks required based
     * on a client, if you need that.
     *
     * Required for OAuth2::GRANT_TYPE_USER_CREDENTIALS.
     *
     * @param $username
     * Username to be check with.
     * @param $password
     * Password to be check with.
     *
     * @return bool
     * TRUE if the username and password are valid, and FALSE if it isn't.
     * Moreover, if the username and password are valid, and you want to
     *
     * @see     http://tools.ietf.org/html/rfc6749#section-4.3
     *
     * @ingroup oauth2_section_4
     */
    public function checkUserCredentials($username, $password)
    {
        $user = self::findOne(['username' => $username]);
        if (empty($user)) {
            return false;
        }

        return Password::validate($password, $user->password_hash);
    }

    /**
     * @return array|false the associated "user_id" and optional "scope" values
     * This function MUST return FALSE if the requested user does not exist or is
     * invalid. "scope" is a space-separated list of restricted scopes.
     * @code
     * return array(
     *     "user_id"  => USER_ID,    // REQUIRED user_id to be stored with the authorization code or access token
     *     "scope"    => SCOPE       // OPTIONAL space-separated list of restricted scopes
     * );
     * @endcode
     */
    public function getUserDetails($username)
    {
        $user = self::find()->select('id')->where(['username' => $username])->one();
        if ($user) {
            return ['user_id' => $user->id];
        }

        return false;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        /** @var OauthAccessTokens $token */
        $token = OauthAccessTokens::find()->where(['access_token' => $token])->select('user_id')->one();
        if ($token) {
            return self::findOne($token->user_id);
        }

        return null;
    }
}
