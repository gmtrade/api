<?php
$config = [
    'id' => 'app-common-tests',
    'basePath' => dirname(__DIR__),
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => 'common\models\User',
        ],
        'request' => [
            'cookieValidationKey' => 'test',
        ],
    ],
];

$configs[] = require __DIR__ . '/main.php';
if (file_exists(__DIR__ . '/main-local.php')) {
    $configs[] = require __DIR__ . '/main-local.php';
}

$configs[] = $config;
if (file_exists(__DIR__ . '/test-local.php')) {
    $configs[] = require __DIR__ . '/test-local.php';
}

return yii\helpers\ArrayHelper::merge(...$configs);
