<?php

$params = [require __DIR__ . '/../../common/config/params.php'];
if (file_exists(__DIR__ . '/../../common/config/params-local.php')) {
    $params[] = require __DIR__ . '/../../common/config/params-local.php';
}

$params[] = require __DIR__ . '/params.php';
if (file_exists(__DIR__ . '/params-local.php')) {
    $params[] = require __DIR__ . '/params-local.php';
}

$params = array_merge(...$params);

return [
    'id'                  => 'app-console',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'console\controllers',
    'aliases'             => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'controllerMap'       => [
        'fixture' => [
            'class'     => 'yii\console\controllers\FixtureController',
            'namespace' => 'common\fixtures',
        ],
        'migrate' => [
            'class'         => 'yii\console\controllers\MigrateController',
            'migrationPath' => [
                '@app/migrations',
                '@vendor/filsh/yii2-oauth2-server/migrations',
                '@vendor/dektrium/yii2-user/migrations',
                '@vendor/miolae/yii2-billing/migrations',
            ],
        ],
    ],
    'components'          => [
        'log' => [
            'targets' => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
    ],
    'params'              => $params,
];
