<?php

use api\tests\ApiTester;
use Codeception\Util\HttpCode;
use api\fixtures\user\ProfileFixture;

class RegisterCest
{
    public function _before(ApiTester $I)
    {
        // Профили подтягивают юзеров, а без профилей юзеры с ошибкой работают
        $I->haveFixtures([ProfileFixture::className()]);
    }

    public function successRegister(ApiTester $I)
    {
        $I->wantTo("register successfully");
        $params = [
            'username' => 'ApiTesterNew',
            'password' => '12345678',
            'email' => 'ApiTesterNew@ApiTester.yii'
        ];
        $I->sendPOST('/users/register', $params);
        $I->seeResponseCodeIs(HttpCode::CREATED);
    }

    public function getMethod(ApiTester $I)
    {
        $I->wantTo('be sure I can\'t register by GET method');
        $params = [
            'username' => 'ApiTester',
            'password' => '12345678',
            'email' => 'ApiTester@ApiTester.yii'
        ];
        $I->sendGET('/users/register', $params);
        $I->seeResponseCodeIs(HttpCode::METHOD_NOT_ALLOWED);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([
            "name" => "Method Not Allowed",
            "message" => "Method Not Allowed. This URL can only handle the following request methods: POST.",
            "code" => 0,
            "status" => 405,
            "type" => "yii\\web\\MethodNotAllowedHttpException"
        ]);
    }

    public function emptyRequest(ApiTester $I)
    {
        $I->wantTo('be sure I can\'t register a new user without a username, password or e-mail');
        $I->sendPOST('/users/register');
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(
            [
                'errors' => [
                    'username' => ['Username cannot be blank.'],
                    'email' => ['Email cannot be blank.'],
                    'password' => ['Password cannot be blank.'],
                ],
            ]
        );
    }

    public function disallowRepeatedRegister(ApiTester $I)
    {
        $I->wantTo("Not to register second time");

        // We have this in fixtures
        $params = [
            'username' => 'ApiTester',
            'password' => '12345678',
            'email' => 'ApiTester@ApiTester.yii'
        ];

        $I->sendPOST('/users/register', $params);
        $I->seeResponseCodeIs(HttpCode::BAD_REQUEST);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(
            [
                'errors' => [
                    'username' => ['This username has already been taken'],
                    'email' => ['This email address has already been taken'],
                ],
            ]
        );

    }
}
