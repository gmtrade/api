<?php

use api\tests\ApiTester;
use api\fixtures\user\oauth\ClientFixture;
use api\fixtures\user\UserFixture;

class TokenCest
{
    /** @var array $postParamsRight Параметры запроса для корректной аутентификации */
    private $postParamsRight = [
        'grant_type' => 'password',
        'username' => 'ApiTester',
        'password' => '12345678',
        'client_id' => 'testclient',
        'client_secret' => 'testpass',
    ];

    public function _before(ApiTester $I)
    {
        $I->haveFixtures([
            'OauthClient' => [
                'class' => ClientFixture::class,
            ],
            'User' => [
                'class' => UserFixture::class,
            ],
        ]);
    }

    private function _checkTokenFailed(ApiTester $I, $title, $params, $answerExpected)
    {
        $I->wantTo("Test receiving token with wrong $title");
        $I->sendPOST('/oauth/token', $params);
        $I->dontSeeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseContainsJson($answerExpected);
    }

    public function authenticateSuccess(ApiTester $I)
    {
        $I->wantTo('Authentication');
        $I->sendPOST('/oauth/token', $this->postParamsRight);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK);
        $I->seeResponseMatchesJsonType([
            'access_token' => 'string',
            'expires_in' => 'integer',
            'token_type' => 'string',
            'scope' => 'null',
            'refresh_token' => 'string',
        ]);
    }

    public function wrongCredentials(ApiTester $I)
    {
        $paramsWrong = [
            'client credentials' => [
                'params' => [
                    'client_id' => 'wrong requisite',
                    'client_secret' => 'wrong requisite',
                ],
                'answerExpected' => [
                    "name" => "Bad Request",
                    "message" => "invalid_client",
                    "code" => 0,
                    "status" => 400,
                    "type" => "filsh\\yii2\\oauth2server\\exceptions\\HttpException"
                ]
            ],
            'user credentials' => [
                'params' => [
//                    'username' => 'wrong username',
                    'password' => 'wrong password',
                ],
                'answerExpected' => [
                    "name" => "Unauthorized",
                    "message" => "invalid_grant",
                    "code" => 0,
                    "status" => 401,
                    "type" => "filsh\\yii2\\oauth2server\\exceptions\\HttpException"
                ]
            ],
            'grant_type' => [
                'params' => [
                    'grant_type' => 'wrong grant type',
                ],
                'answerExpected' => [
                    "name" => "Bad Request",
                    "message" => "unsupported_grant_type",
                    "code" => 0,
                    "status" => 400,
                    "type" => "filsh\\yii2\\oauth2server\\exceptions\\HttpException"
                ]
            ],
        ];

        foreach ($paramsWrong as $group => $block) {
            foreach ($block['params'] as $key => $value) {
                $params = $this->postParamsRight;
                $params[$key] = $value;
                $this->_checkTokenFailed($I, $key, $params, $block['answerExpected']);
            }

            if (count($block['params']) > 1) {
                $params = \yii\helpers\ArrayHelper::merge($this->postParamsRight, $block['params']);
                $params['client_id'] = 'wrong client id';
                $this->_checkTokenFailed($I, $group, $params, $block['answerExpected']);
            }
        }
    }
}
