<?php

namespace api\controllers;

use dektrium\user\models\RegistrationForm;
use dektrium\user\Module as UserModule;
use dektrium\user\traits\EventTrait;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;

class UsersController extends Controller
{
    use EventTrait;

    /**
     * Event is triggered after creating RegistrationForm class.
     * Triggered with \dektrium\user\events\FormEvent.
     */
    const EVENT_BEFORE_REGISTER = 'beforeRegister';

    /**
     * Event is triggered after successful registration.
     * Triggered with \dektrium\user\events\FormEvent.
     */
    const EVENT_AFTER_REGISTER = 'afterRegister';

    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'register' => ['POST'],
                ],
            ],
        ];

        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }

    /**
     * @return array|null
     * @throws NotFoundHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRegister()
    {
        $module = \Yii::$app->getModule('user');
        /** @var UserModule $module */
        /** @noinspection NullPointerExceptionInspection */
        if (!$module->enableRegistration) {
            throw new NotFoundHttpException();
        }

        \Yii::$app->response->statusCode = 201;

        /** @var RegistrationForm $model */
        $model = \Yii::createObject(RegistrationForm::class);

        $event = $this->getFormEvent($model);
        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);

        $model->load(\Yii::$app->request->post(), '');
        if ($model->register()) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);
        } else {
            \Yii::$app->response->statusCode = 400;

            return [
                'errors' => $model->errors,
            ];
        }

        return null;
    }
}

