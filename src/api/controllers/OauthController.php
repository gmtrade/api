<?php

namespace api\controllers;

use filsh\yii2\oauth2server\controllers\RestController;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class OauthController extends RestController
{
    public function init()
    {
        $this->module = \Yii::$app->getModule('oauth2');
    }

    public function behaviors()
    {
        $behaviors = [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'register' => ['POST', 'OPTIONS'],
                ],
            ],
        ];

        return ArrayHelper::merge(parent::behaviors(), $behaviors);
    }
}
