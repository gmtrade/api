<?php

use dektrium\user\helpers\Password;

return [
    'ApiTester' => [
        'username'      => 'ApiTester',
        'email'         => 'ApiTester@ApiTester.yii',
        'password_hash' => Password::hash(12345678),
        'auth_key'      => 'test',
        'created_at'    => 1,
        'updated_at'    => 1,
    ],
    'forNotes'  => [
        'username'      => 'forNotes',
        'email'         => 'NotesTester@ApiTester.yii',
        'password_hash' => Password::hash(12345678),
        'auth_key'      => 'test',
        'created_at'    => 1,
        'updated_at'    => 1,
    ],
];
