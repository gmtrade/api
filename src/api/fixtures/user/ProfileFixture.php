<?php

namespace api\fixtures\user;

use common\fixtures\BaseFixture;

class ProfileFixture extends BaseFixture
{
    public $modelClass = '\dektrium\user\models\Profile';
    public $dataFile = 'profiles.php';
    public $depends = ['\api\fixtures\user\UserFixture'];
}
