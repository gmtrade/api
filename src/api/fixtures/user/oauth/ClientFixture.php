<?php

namespace api\fixtures\user\oauth;

use common\fixtures\BaseFixture;
use filsh\yii2\oauth2server\models\OauthClients;

class ClientFixture extends BaseFixture
{
    public $modelClass = OauthClients::class;
    public $dataFile = 'clients.php';
}
