<?php

namespace api\fixtures\user;

use common\fixtures\BaseFixture;

class UserFixture extends BaseFixture
{
    public $modelClass = 'common\models\User';
    public $dataFile = 'users.php';
}
