<?php

namespace api\fixtures\user;

use common\fixtures\BaseFixture;

class TokenFixture extends BaseFixture
{
    public $modelClass = 'common\models\User';
    public $dataFile = 'tokens.php';
}
