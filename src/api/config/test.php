<?php

$config = [
    'id' => 'app-api-tests',
    'components' => [
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager' => [
            'showScriptName' => true,
        ],
    ],
];

$configs = [require __DIR__ . '/../../common/config/test.php'];
if (file_exists(__DIR__ . '/../../common/config/test-local.php')) {
    $configs[] = require __DIR__ . '/../../common/config/test-local.php';
}

$configs[] = require __DIR__ . '/main.php';
if (file_exists(__DIR__ . '/main-local.php')) {
    $configs[] = require __DIR__ . '/main-local.php';
}

$configs[] = $config;
if (file_exists(__DIR__ . '/test-local.php')) {
    $configs[] = require __DIR__ . '/test-local.php';
}

return yii\helpers\ArrayHelper::merge(...$configs);
