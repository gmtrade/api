<?php

use common\models\User;
use filsh\yii2\oauth2server\Module;
use OAuth2\GrantType\UserCredentials;
use yii\log\FileTarget;

$params = [require __DIR__ . '/../../common/config/params.php'];
if (file_exists(__DIR__ . '/../../common/config/params-local.php')) {
    $params[] = require __DIR__ . '/../../common/config/params-local.php';
}

$params[] = require __DIR__ . '/params.php';
if (file_exists(__DIR__ . '/params-local.php')) {
    $params[] = require __DIR__ . '/params-local.php';
}

$params = array_merge(...$params);

/** @noinspection SummerTimeUnsafeTimeManipulationInspection */
return [
    'id'                  => 'app-api',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'api\controllers',
    'components'          => [
        'request'      => [
            'csrfParam' => '_csrf-api',
            'baseUrl'   => '/api',
        ],
        'user'         => [
            'identityClass'   => User::class,
            'enableAutoLogin' => true,
            'identityCookie'  => ['name' => '_identity-api', 'httpOnly' => true],
        ],
        'session'      => [
            // this is the name of the session cookie used for login on the api
            'name' => 'advanced-api',
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets'    => [
                [
                    'class'  => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => [
            ],
        ],
    ],
    'modules'             => [
        'oauth2' => [
            'class'               => Module::class,
            'tokenParamName'      => 'accessToken',
            'tokenAccessLifetime' => 3600 * 24,
            'storageMap'          => [
                'user_credentials' => User::class,
            ],
            'grantTypes'          => [
                'user_credentials' => [
                    'class' => UserCredentials::class,
                ],
            ],
        ],
    ],
    'params'              => $params,
];
