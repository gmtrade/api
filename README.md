Disclaimer: MySql запускается еще некоторое время после старта сервера. После команды `make up` необходимо выждать до 40 секунд прежде, чем начинать работать с БД.
 
 Например, такая команда не пройдет: `make up test` (к этому моменту MySql еще не запустился, а мы уже пытаемся провести миграции)
 
## Как начать работать с проектом

1. `git clone`
1. Скопировать файл `env.example` в `.env`. Заменить все неподходящие значения на нужные сейчас. При необходимости можно дописать туда строки:
    1. `BUILD_TAG = latest` - какой тэг использовать для основного докер-образа. Последний стабильный - это `latest` или `master`, так же тэг с названием git-ветки (нижний регистр, `-` вместо пробелов) указывает на билд с последним коммитом ветки.
    1. `WEB_PORT = 80` - можно указать кастомный порт для веб-сервера, если 80 у вас уже занят
1. Выполнить команду `make pull up` (для windows - `docker-compose pull` и `docker-compose up -d --remove-orphans`)
1. При необходимости активировать gii:
    1. Создать файл `src/common/config/main-local.php`
    1. Вставить туда следующее содержимое: 
        ```php
        $config = [];
    
        if (YII_ENV_DEV) {
            $config['bootstrap'][] = 'gii';
            $config['modules']['gii'] = [
                'class' => 'yii\gii\Module',
                'allowedIPs' => ['*']
            ];
        }
        
        return $config;
        ```
    
    

## Команды из Makefile

1. Поднять сервера:
    - nix: команда `make up`
    - win: команда `docker-compose up -d --remove-orphans`
1. Запустить тесты:
    - nix: `make test`
    - win: последовательность команд
        - `docker-compose exec app php yii migrate/fresh --interactive=0`
        - `docker-compose exec app php vendor/bin/codecept build`
        - `docker-compose exec -d app php ./yii serve -t @api/web`
        - `docker-compose exec app php -d xdebug.remote_autostart=1 vendor/bin/codecept run`
1. Остановить все контейнеры (сервера):
    - nix: `make down`
    - win: `docker-compose down`
1. Выполнить команду в контейнере с php:
    - nix: `make exec c='команда'` (напр., `make exec c='ls -la'`) 
    - win: `docker-compose exec app команда` (напр., `docker-compose exec app ls -la`)
1. Выполнить команду для файла `yii`:
    - nix: `make yii c='команда'` (напр., `make yii c=migrate`)
    - win: `docker-compose exec app php yii команда` (напр., `docker-compose exec app php yii migrate`)
1. Поднять http-сервер для приложения. После этого можнообращаться к нему по адресу [http://localhost:8080](http://localhost:8080):
    - nix: `make serve`
    - win: `docker-compose exec app php ./yii serve -t @api/web --interactive`
1. Поднять http-сервер для приложения _в фоне_:
    - nix: `make serve-detached`
    - win: `docker-compose exec -d app php ./yii serve -t @api/web`
1. **Посмотреть, будет ли твой код работать на проде** (не забыть про 40 секунд). Это нужно, т.к. локально (и под `make up`) могут оказаться файлы, не отслеживаемые гитом. Этих файлов на проде не будет, если в процессе билда образа мы их не добавим. Необходимо проверять, что либо мы добавили их корректную обработку, либо работает и без них. 
    - nix: `make build up-test test`
    - win:
        - `docker build --pull -t viktorprogger/gm-trade-api:<название-ветки> -f .docker/php/Dockerfile ./src`
        - `BUILD_TAG=<название-ветки> docker-compose -f docker-compose.ci.yml up -d --remove-orphans`
        - `docker-compose exec app php yii migrate/fresh --interactive=0`
        - `docker-compose exec app php vendor/bin/codecept build`
        - `docker-compose exec -d app php ./yii serve -t @api/web`
        - `docker-compose exec app php -d vendor/bin/codecept run`
1. Посмотреть, какие еще команды и комплексы команд есть в `Makefile`:
    - команда `make` (отобразится хелп)
    - открыть файл `Makefile` (и почитать его)
